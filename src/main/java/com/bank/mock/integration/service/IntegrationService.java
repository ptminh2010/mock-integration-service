package com.bank.mock.integration.service;

import com.bank.mock.integration.model.ReservedLimitResponse;
import com.bank.mock.integration.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IntegrationService {
    public String integrate(String request) {
        ReservedLimitResponse reservedLimit =
                (ReservedLimitResponse) ObjectUtil.stringToObject(request, ReservedLimitResponse.class);
        if (reservedLimit.getTransferRequest().getDemoFail()) {
            log.warn("throw exception to demo SAGA");
            throw new RuntimeException("This is an integration exception");
        }
        return "Transaction " + reservedLimit.getTransferRequest().getClientTransactionId() + " Processed";
    }
}
