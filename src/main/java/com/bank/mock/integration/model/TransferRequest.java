package com.bank.mock.integration.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

@Setter
@Getter
public class TransferRequest implements Serializable {
    @Schema(
            name = "clientTransactionId",
            title = "clientTransactionId",
            example = "a51fea9c-ea7a-4a02-bfd4-0eec3aa00000"
    )
    private UUID clientTransactionId;

    @Schema(
            name = "productId",
            title = "The Product ID",
            example = "a51fea9c-ea7a-4a02-bfd4-0eec3aa55a5a"
    )
    private UUID productId;

    @Schema(
            name = "productCode",
            title = "The Product ID",
            example = "INTERNAL_TRANSFER"
    )
    private String productCode;

    @Schema(
            name = "fromCif",
            title = "From CIF (User_ID)",
            example = "user_1"
    )
    private String fromCif;

    @Schema(
            name = "beneficiary",
            title = "To Beneficiary",
            example = "user_4"
    )
    private String beneficiary;

    @Schema(
            name = "amount",
            title = "Transfer Amount",
            example = "1000"
    )
    private BigDecimal amount;

    private Boolean demoFail;

    @Override
    public String toString() {
        return "TransferRequest{" +
                "clientTransactionId=" + clientTransactionId +
                ", productId=" + productId +
                ", productCode='" + productCode + '\'' +
                ", fromCif='" + fromCif + '\'' +
                ", beneficiary='" + beneficiary + '\'' +
                ", amount=" + amount +
                ", demoFail=" + demoFail +
                '}';
    }
}
